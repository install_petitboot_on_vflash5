
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <psl1ght/lv2/net.h>

#include <lv2_syscall.h>
#include <udp_printf.h>

#define VFLASH5_DEV_ID				0x100000500000001ull
#define VFLASH5_SECTOR_SIZE			0x200ull
#define VFLASH5_SECTORS				0xc400ull
#define VFLASH5_HEADER_SECTORS			0x2ull
#define VFLASH5_OS_DB_AREA_SECTORS		0x2ull

#define HEADER_MAGIC				"cell_ext_os_area"
#define HEADER_VERSION				1
#define HEADER_LDR_FORMAT_RAW			0

#define PETITBOOT_FILENAME			"dtbImage.ps3.bin"

struct os_area_header {
	uint8_t magic[16];
	uint32_t version;
	uint32_t db_area_offset;
	uint32_t ldr_area_offset;
	uint32_t res1;
	uint32_t ldr_format;
	uint32_t ldr_size;
	uint32_t res2[6];
};

static const char *petitboot_path[] = {
	"/dev_usb000/" PETITBOOT_FILENAME,
	"/dev_usb001/" PETITBOOT_FILENAME
};

/*
 * open_petitboot
 */
static FILE *open_petitboot(void)
{
#define N(a)	(sizeof(a) / sizeof(a[0]))

	FILE *fp;
	int i;

	fp = NULL;

	for (i = 0; i < N(petitboot_path); i++) {
		PRINTF("%s:%d: trying path '%s'\n", __func__, __LINE__, petitboot_path[i]);

		fp = fopen(petitboot_path[i], "r");
		if (fp)
			break;
	}

	if (fp)
		PRINTF("%s:%d: path '%s'\n", __func__, __LINE__, petitboot_path[i]);
	else
		PRINTF("%s:%d: file not found\n", __func__, __LINE__);

	return fp;

#undef N
}

/*
 * main
 */
int main(int argc, char **argv)
{
#define MIN(a, b)	((a) <= (b) ? (a) : (b))

	uint32_t dev_handle;
	FILE *fp;
	int file_size, file_sectors, start_sector, sector_count;
	uint8_t buf[VFLASH5_SECTOR_SIZE * 16];
	struct os_area_header *hdr;
	uint32_t unknown2;
	int result;

	netInitialize();

	udp_printf_init();

	PRINTF("%s:%d: start\n", __func__, __LINE__);

	dev_handle = 0;
	fp = NULL;

	fp = open_petitboot();
	if (!fp)
		goto done;

	result = lv2_storage_open(VFLASH5_DEV_ID, &dev_handle);
	if (result) {
		PRINTF("%s:%d: lv2_storage_open failed (0x%08x)\n", __func__, __LINE__, result);
		goto done;
	}

	result = fseek(fp, 0, SEEK_END);
	if (result) {
		PRINTF("%s:%d: fseek failed (0x%08x)\n", __func__, __LINE__, result);
		goto done;
	}

	file_size = ftell(fp);
	file_sectors = (file_size + VFLASH5_SECTOR_SIZE - 1) / VFLASH5_SECTOR_SIZE;

	if (file_sectors > (VFLASH5_SECTORS - VFLASH5_HEADER_SECTORS - VFLASH5_OS_DB_AREA_SECTORS)) {
		PRINTF("%s:%d: file too large\n", __func__, __LINE__);
		goto done;
	}

	PRINTF("%s:%d: file size (0x%08x) sectors (0x%08x)\n", __func__, __LINE__, file_size, file_sectors);

	result = fseek(fp, 0, SEEK_SET);
	if (result) {
		PRINTF("%s:%d: fseek failed (0x%08x)\n", __func__, __LINE__, result);
		goto done;
	}

	/* write os header and db area */

	start_sector = 0;
	sector_count = VFLASH5_HEADER_SECTORS + VFLASH5_OS_DB_AREA_SECTORS;

	PRINTF("%s:%d: writing header start_sector (0x%08x) sector_count (0x%08x)\n",
		__func__, __LINE__, start_sector, sector_count);

	memset(buf, 0, sizeof(buf));
	hdr = (struct os_area_header *) buf;
	strncpy((char *) hdr->magic, HEADER_MAGIC, sizeof(hdr->magic));
	hdr->version = HEADER_VERSION;
	hdr->db_area_offset = VFLASH5_HEADER_SECTORS; /* in sectors */
	hdr->ldr_area_offset = VFLASH5_HEADER_SECTORS + VFLASH5_OS_DB_AREA_SECTORS; /* in sectors */
	hdr->ldr_format = HEADER_LDR_FORMAT_RAW; /* we do not use gzip format !!! */
	hdr->ldr_size = file_size;

	result = lv2_storage_write(dev_handle, 0, start_sector, sector_count, buf, &unknown2, 0);
	if (result) {
		PRINTF("%s:%d: lv2_storage_write failed (0x%08x)\n", __func__, __LINE__, result);
		goto done;
	}

	start_sector += VFLASH5_HEADER_SECTORS + VFLASH5_OS_DB_AREA_SECTORS;

	while (file_sectors) {
		sector_count = MIN(file_sectors, 16);

		result = fread(buf, 1, sector_count * VFLASH5_SECTOR_SIZE, fp);
		if (result < 0) {
			PRINTF("%s:%d: fread failed (0x%08x)\n", __func__, __LINE__, result);
			goto done;
		}

		PRINTF("%s:%d: writing data start_sector (0x%08x) sector_count (0x%08x)\n",
			__func__, __LINE__, start_sector, sector_count);

		result = lv2_storage_write(dev_handle, 0, start_sector, sector_count, buf, &unknown2, 0);
		if (result) {
			PRINTF("%s:%d: lv2_storage_write failed (0x%08x)\n", __func__, __LINE__, result);
			goto done;
		}

		usleep(10000);

		file_sectors -= sector_count;
		start_sector += sector_count;
	}

	PRINTF("%s:%d: end\n", __func__, __LINE__);

done:

	if (fp)
		fclose(fp);

	result = lv2_storage_close(dev_handle);
	if (result)
		PRINTF("%s:%d: lv2_storage_close failed (0x%08x)\n", __func__, __LINE__, result);

	udp_printf_deinit();

	netDeinitialize();

	return 0;

#undef MIN
}
